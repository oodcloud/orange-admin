package com.orangeforms.webadmin.upms.dao;

import com.orangeforms.common.core.base.dao.BaseDaoMapper;
import com.orangeforms.webadmin.upms.model.SysPermCodePerm;

/**
 * 权限字与权限资源关系数据访问操作接口。
 *
 * @author Jerry
 * @date 2022-02-20
 */
public interface SysPermCodePermMapper extends BaseDaoMapper<SysPermCodePerm> {
}
